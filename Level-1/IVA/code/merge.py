import os
import datetime
import collections
from ffmpy import FFmpeg

os.chdir('../input/IVA')
folders = os.listdir()

# Sort the folders according to dates
folders = sorted(folders, key=lambda x: datetime.datetime.strptime(x, '%d.%m.%Y'))

files = []
mp4_files = []

# Iterate throught the folders
for folder in folders:
	subfolders = os.listdir(folder)
	subfolders = sorted(subfolders, key=lambda x: int(x))
	for subfolder in subfolders:
		files.append(folder + '/' + subfolder + '/' + 'archive.iva')
		mp4_files.append(folder + '/' + subfolder + '/' + 'archive.mp4')


# Make file list for concatinatiion
listfile = open('list.txt','w')

for f in mp4_files:
	listfile.write('file \'' + f + '\'\n')

listfile.close()

#convert each file into mp4 first
for x in range(len(files)):
	ff = FFmpeg(
	    inputs={files[x]: None},
	    outputs={mp4_files[x]: None},
	)
	ff.run()

ff = FFmpeg(
    inputs={'list.txt': None},
    outputs={'../../output/merged.mp4': '-c copy'},
    global_options='-f concat'
)
ff.run()

# Clean up
os.remove('list.txt')
for f in mp4_files:
	os.remove(f)