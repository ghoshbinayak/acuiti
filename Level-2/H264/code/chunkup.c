#include <stdio.h>
#include <stdlib.h>

long filelen;

char * readbytes(int n, FILE * f){
	char c;
	if((c = fgetc(f)) == EOF){
		return NULL;
	}
	ungetc(c, f);

	char * buff = (char *)malloc(n);
	fread(buff, n, 1, f);
	filelen -= n;
	return buff;
}

int main(){
	FILE *filein;
	FILE * fout0;
	FILE * fout1;
	FILE * fout2;
	FILE * fout3;
	unsigned char *buffer;
	int len, ch;

	filein = fopen("../input/20140726_040000_ps.h264", "rb");  // Open the file in binary mode
	fout0 = fopen("chunk0.bin", "wb");  
	fout1 = fopen("chunk1.bin", "wb");  
	fout2 = fopen("chunk2.bin", "wb");  
	fout3 = fopen("chunk3.bin", "wb");  
	fseek(filein, 0, SEEK_END);          // Jump to the end of the file
	filelen = ftell(filein);             // Get the current byte offset in the file
	rewind(filein);

	// Skip the header
	buffer = readbytes(1428, filein);
	free(buffer);


	while(filelen){
		// Read the flags
		buffer = readbytes(24, filein);

		// Find the length of the chunk
		len = (buffer[21] << 8) | buffer[20];

		// Find the channel
		ch = buffer[0];

		free(buffer);
		// printf("Length: %d, channel: %d, %02x %02x \n", len, ch, buffer[20], buffer[21]);

		// Read chunk
		buffer = readbytes(len, filein);	
		// write to file
		switch(ch){
			case 0:
				fwrite(buffer, len, 1, fout0);
				break;
			case 1:
				fwrite(buffer, len, 1, fout1);
				break;
			case 2:
				fwrite(buffer, len, 1, fout2);
				break;
			case 3:
				fwrite(buffer, len, 1, fout3);
				break;
		}
		free(buffer);
	}  

	fclose(filein);
	fclose(fout0);
	fclose(fout1);
	fclose(fout2);
	fclose(fout3);
	return 0;
}